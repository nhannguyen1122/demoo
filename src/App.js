import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
  useHistory,
  useLocation
} from "react-router-dom";
import DetailsData from './component/Details';
import LoginComponent from './component/login';
function App() {
  return (
    <div className="App">
 
    
      <Router>
          <Switch>
            <Route path="/">
              <LoginComponent />
            </Route>
            <PrivateRoute path="/data">
              <DetailsData />
            </PrivateRoute>
          </Switch>
      </Router>


    </div>
    
  );
}
function PrivateRoute({ children, ...rest }) {
  let auth = localStorage.getItem('token');
  return (
    <Route
      {...rest}
      render={({ location }) =>
        auth? (
          children
        ) : (
          <Redirect
            to={{
              pathname: "/",
              state: { from: location }
            }}
          />
        )
      }
    />
  );
}
export default App;
