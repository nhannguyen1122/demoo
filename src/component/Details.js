import React, { useState } from 'react';
import MaterialTable from 'material-table'

function DetailsData(props) {
    const[loading,setLoading]=useState(false);
    const[data,setData]=useState([]);
    const columns=[
        {
            title:'id',
            field:'index',
        },
        {
            title:'name',
            field:'Name'
        }
    ]
    return (
       <MaterialTable
       columns={columns}
       isLoading={loading}
       data={data}

       />
    );
}

export default DetailsData;