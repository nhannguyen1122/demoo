import { Button, MenuItem, Select, TextField } from "@material-ui/core";
import Axios from "axios";
import { FastField, Form, Formik } from "formik";
import React, { useEffect, useState } from "react";
import FormControl from "@material-ui/core/FormControl";

import "../css/login.css";
const LoginComponent = (prop) => {
  const server = [{ value: "EUW" }, { value: "NA" }, { value: "EUN" }];
  const urlgetName = `https://na1.api.riotgames.com/tft/summoner/v1/summoners/by-name/`;
  const urlgetId = `https://na1.api.riotgames.com/lol/platform/v4/third-party-code/by-summoner`;
  const fetchApi = async (url, data) => {
    let res = await Axios({
      method: "post",
      url: "https://dev-api.tornomy.com/v1/public/forward",
      data: {
        url: `${url}${data}`,
        method: "get",
        header: JSON.stringify({
          "X-Riot-Token": "RGAPI-e8d52d95-2313-447a-b9c2-bd676b2f2020"
        })
      },
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
        "Accept": "application/json"
      }
    });
    return res;
  };

  //submit form data
  const handleSubmitValue = async ({ server, name, code }) => {
      console.log(server,name,code);
    let result = await fetchApi(urlgetName, name);
    //fetchId
    console.log(result);
    const {id, accountId, puuid } = result.data.data;
    //getcode
    let getVerifyCode = await fetchApi(
      urlgetId,accountId
    );
    //verify token
    
    const{token}=getVerifyCode.data;
    if(code===token){
        localStorage.setItem("token");
    }
    

    
    //
  };
  return (
    <div className="login-box">
      <h1>Connect your account lol</h1>
      <Formik
        initialValues={{
          server: "",
          name: "",
          code: ""
        }}
        validateSchema={{}}
        onSubmit={(value) => handleSubmitValue(value)}
      >
        {(props) => {
          return (
            <Form>
              <label>
                Server
              </label>
                <FastField
                 component="select"
                className="select"
                  name="server"
                  label="server"
                >
                  {server.map((item) => (
                    <option className="option" value={item.value}>{item.value}</option>
                  ))}
                </FastField>
              <label>Summoner Name</label>
              <div className="user-box">
                <div>
                  <FastField name="name" type="input" label="server" />
                </div>
              </div>
              <label>
                Your 3rd vertification code
              </label>
              <div className="user-box">
                <div>
                  <FastField name="code" type="input" label="server" />
                </div>
              </div>
              <Button
                style={{
                  color: "orange",
                  backgroundColor: "yellow",
                  width: "100%"
                }}
                type="submit"
              >
                connect
              </Button>
            </Form>
          );
        }}
      </Formik>
    </div>
  );
};
export default LoginComponent;
